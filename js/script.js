// lest to store user email
var EmailList  = [];

// all user
let usersObject ;

// user opend in a model 
let userInModel;

// add alphabetical to slect in serch bar
document.addEventListener('DOMContentLoaded', function() {

	usersObject = JSON.parse(localStorage.getItem('users')) || [];

	for (let index = 0; index < usersObject.length; index++) {
		// push Exesit email to EmailList  and create Element
		EmailList.push(usersObject[index].email);

		// re  render the dom 
		appendUser(
			usersObject[index].name,
			usersObject[index].email, 
			usersObject[index].mojer ,
			usersObject[index].role ,
			usersObject[index].bio);
	}

	document.getElementById('numberOfItems').innerHTML = usersObject.length+" items";
}, false);

// DOM 
/**
 * re render the dom 
 */
function reRenderDOM(){
	// console.log(usersObject.length);
	document.getElementById('Members').innerHTML = "";
	for (let index = 0; index < usersObject.length; index++) {
		appendUser(
			usersObject[index].name,
			usersObject[index].email, 
			usersObject[index].mojer ,
			usersObject[index].role ,
			usersObject[index].bio);
	}
}


/**
 * @param userName as String 
 * @param email as string  
 * @param mojer as string 
 * @param role as sting 
 * @param bio as sting
 * append new users to users list 
 */

function appendUser(userName , email ,  mojer,role, bio){
	let members = document.getElementById('Members');
	members.appendChild(createMunisButtonDiv(email));
	members.appendChild(createUserDiv(userName,email, mojer , role ,bio));
}

/**
 * @param userName as String 
 * @param email as string  
 * @param mojer as string 
 * @param role as sting 
 * @param bio as sting
 * @returns return new user div 
 */
function createUserDiv(userName , email ,  mojer,role, bio){
	let user_div = document.createElement('div');

	let user_name = document.createElement('h1');
	user_name.setAttribute('class','user-name');
	user_name.innerHTML = userName;

	let user_data = document.createElement('h3');
	user_data.setAttribute('class' , 'user-data');
	user_data.innerHTML = email + "/" + mojer + "/" + role;

	let user_bio = document.createElement('user-bio');
	user_bio.setAttribute('class' , 'user-bio');
	user_bio.innerHTML = bio; 

	user_div.appendChild(user_name);
	user_div.appendChild(user_data);
	user_div.appendChild(user_bio);
	user_div.setAttribute('class','user-div')
	user_div.onclick = function() { openModel(userName , email ,  mojer,role, bio) ;};
	return user_div;
}
 
/**
 * @param email as string
 * @returns return new munis button
 */
function createMunisButtonDiv(email){
	let minus_button_div = document.createElement('div');
	let minus_button = document.createElement('button');
	minus_button.setAttribute('class','minus-button');
	minus_button.setAttribute('id',email);
	// delete the element
	minus_button.onclick = function(){
		deleteUser(this.id);
	};
	let minus_sagin = document.createElement('span');
	minus_sagin.setAttribute('class' , 'minus-sagin');
	minus_sagin.innerHTML = "&#8211;";

	minus_button.appendChild(minus_sagin);
	minus_button_div.appendChild(minus_button);
	return minus_button_div;
}


// add and delete 
function SaveDate(){
	// get Data From Page
	let userName = document.getElementById('InputUserName').value;
	let email = document.getElementById('InputEmail').value;
	let mojer = document.getElementById('InputMojer').value;
	let role = document.getElementById('InputRole').value;
	let bio = document.getElementById('InputBIO').value;

	// Chack if data is valid
	let IsValidUserNmae = CheckValidUserName(userName);
	let IsValidEmail = CheckValidEmail(email);
	let IsValidBIO = checkValidBIO(bio);
	
	if(IsValidUserNmae && IsValidEmail && IsValidBIO
		&& mojer != "none" && role != "none"){
		usersObject = JSON.parse(localStorage.getItem('users'));
		document.getElementById('char_serch').value = 'none';
		document.getElementById('major_serch').value = 'none';
		document.getElementById('role_serch').value = 'none';
		document.getElementById('serchByname').value = '';
		if(document.getElementById('appendUser').checked){
			usersObject.push( {
				name : userName,
				email : email,
				mojer : mojer,
				role:role ,
				bio : bio,
				dataTime : new Date()
			});
		}
		else if(validIndexToInsertAt()){
			let index = parseInt(document.getElementById('InsertAtindex').value);
			usersObject.splice(index-1, 0, {
				name : userName,
				email : email,
				mojer : mojer,
				role:role ,
				bio : bio,
				dataTime : new Date()
			});
		}else {
			usersObject.unshift( {
				name : userName,
				email : email,
				mojer : mojer,
				role:role ,
				bio : bio,
				dataTime : new Date().getUTCDay()
			});
		}
		EmailList.push(email);
		// re render the dom
		reRenderDOM();
		// add new item to local storage
		localStorage.setItem("users",JSON.stringify(usersObject));
		// update number of user feld 
		document.getElementById('numberOfItems').innerHTML = usersObject.length+" items";
	}else{
		let errorMSG = "";
		if(!IsValidUserNmae) errorMSG += "user name is reqerd\n";
		if(!IsValidEmail) errorMSG += "email already exist or  format it is invalid\n";
		if(!IsValidBIO) errorMSG += "bio between 500 and 1500\n";
		if(mojer == "none") errorMSG += "please choose  an major\n";
		if(role == "none") errorMSG += "please choose  an role";
		alert(errorMSG);
	}
	
}

/**
 * @param email as string
 * delete user according to its email
 */
function deleteUser(email){
	usersObject = JSON.parse(localStorage.getItem('users'));
	document.getElementById('char_serch').value = 'none';
	document.getElementById('major_serch').value = 'none';
	document.getElementById('role_serch').value = 'none';
	document.getElementById('serchByname').value = '';
	// delete user email form EmailList
	for (let index = 0; index < EmailList.length; index++) {
		if(EmailList[index] === email){
			EmailList.splice(index,1);
			break;
		}
		
	}
	// delte the user 
	for( let index = 0; index < usersObject.length; index++){ 
		if ( usersObject[index].email  === email) {
		  usersObject.splice(index, 1); 
		  // re render the dom
		  reRenderDOM();
		  // add new item to local storage
		   localStorage.setItem("users",JSON.stringify(usersObject));
		   // update number of user feld 
			document.getElementById('numberOfItems').innerHTML = usersObject.length+" items";
		   break;
		}
	 }
}

// filter 
function fillterAll(){
	usersObject = JSON.parse(localStorage.getItem('users'));
	let sort = sorteData();
	let names = serchByNameFunc();
	let majors = serchByMajorFunc();
	let roles = serchByRoleFunc();

	
	if(sort &&names &&majors && roles){
		usersObject = JSON.parse(localStorage.getItem('users'));
	}
	// update number of user feld 
	document.getElementById('numberOfItems').innerHTML = usersObject.length+" items";
	reRenderDOM();
}
// order data 
function sorteData(){
	let type = document.getElementById('char_serch').value;

	if(type == "none"){
		return true;
	}
	if(type == "ascending"){
		usersObject.sort(function(first , second){
			if (first.name.toUpperCase() > second.name.toUpperCase()) {
				return 1;
			}
			if (second.name.toUpperCase() > first.name.toUpperCase()) {
				return -1;
			}
			return 0;
		});
	}
	else if(type == "descending") {
		usersObject.sort(function(first , second){
			if (first.name.toUpperCase() > second.name.toUpperCase()) {
				return -1;
			}
			if (second.name.toUpperCase() > first.name.toUpperCase()) {
				return 1;
			}
			return 0;
		});
	}
	return false;
}

// filter by name 
function serchByNameFunc(){
	let word = document.getElementById('serchByname').value;
	let pattern = new RegExp('(\\w*'+word+'\\w*)','gi');

	if(word == "")return true;
	usersObject = usersObject.filter((user) => user.name.match(pattern)!== null);
	return false;
}

//filter by major
function serchByMajorFunc(element){
	let type = document.getElementById('major_serch').value;
	var typeRole = document.getElementById('role_serch').value;

	if(type == "none")return true;

		usersObject = usersObject.filter((user) => user.mojer == type);

		if(typeRole != "none"){
			usersObject = usersObject.filter((user) => user.role == typeRole);
		}
		return false;
}

// filter by role 
function serchByRoleFunc(element){
	var type = document.getElementById('role_serch').value;
	var typeMajor = document.getElementById('major_serch').value;

	if(type == "none")return true;
	usersObject = usersObject.filter((user) => user.role == type);

	if(typeMajor != "none"){
		usersObject = usersObject.filter((user) => user.mojer == typeMajor);
	}
	return false;

}

// model 
///// model \\\\\

function saveFromModel(){
	let userName  = document.getElementById('model_userName').value;
	let email = document.getElementById('model_email').value;
	let mojer =  document.getElementById('model_major').value;
	let role =  document.getElementById('model_role').value;
	let bio  =  document.getElementById('model_userBIO').value;
	
	// Chack if data is valid
	let IsValidUserNmae = CheckValidUserName(userName);
	let IsValidBIO = checkValidBIO(bio);
	if(IsValidUserNmae  && IsValidBIO ){
		for (let i = 0; i < usersObject.length; i++) {
			if(usersObject[i].email == userInModel.email){
				usersObject[i].name  = userName;
				usersObject[i].email = email;
				usersObject[i].mojer = mojer;
				usersObject[i].role =  role;
				usersObject[i].bio  =  bio;
				break;
			}
		}
		for(let i = 0 ; i < EmailList.length;i++){
			if(EmailList[i] == userInModel.email){
				EmailList[i] = document.getElementById('model_email').value;
			}
		}
		localStorage.setItem('users',JSON.stringify(usersObject));
		reRenderDOM();

		closeModel();
	}else {
		let errorMSG = "";
		if(!IsValidUserNmae) errorMSG += "user name is reqerd\n";
		if(!IsValidBIO) errorMSG += "bio between 500 and 1500\n";
		alert(errorMSG);
	}
}

/**
 * @param userName as String 
 * @param email as string  
 * @param mojer as string 
 * @param role as sting 
 * @param bio as sting
 * open model with send data  
 */
function openModel(userName,email,mojer,role,bio){
	userInModel = {name:userName, email:email , mojer:mojer , role:role , bio : bio };

	document.getElementById('model_userName').value = userName; 
	document.getElementById('model_email').value = email;
	document.getElementById('model_major').value = mojer;
	document.getElementById('model_role').value = role ;
	document.getElementById('model_userBIO').value = bio;
	document.getElementById('model_id').style.display = 'block';
}

// delte data using model
function delteFromModel(){
	deleteUser(document.getElementById('model_email').value);
	closeModel();
}

// close model using buttom
function closeModel(){
	userInModel = {}
	document.getElementById('model_id').style.display = 'none';
}

// close model using windo 
window.addEventListener('click', function(e){
	if(e.target === document.getElementById('model_id')){
		closeModel();
	}
});

// validation 
/**
 * @param BIO as string 
 * @returns return true if BIO length  is between 500 and 1500  else return false
 */
function checkValidBIO(BIO){
	if(BIO == null || typeof BIO != "string"){
		return false;
	}

	if(BIO.length>=500&&BIO.length<=1500){
		return true;
	}
	return false;
}

/**
 * @param email as string 
 * @returns return false if email is invalid form or it's already exist else return true
 */
function CheckValidEmail(email){
	if(email == null || typeof email != "string"){
		return false;
	}
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	
	if(!mailformat.test(email)){
		return false;
	}

	let find = false;
	EmailList.forEach(element => {
		if(element == email){find = true;return;}
	});
	if(find)return false;
		

	return true;
}


/**
 * @param userName as string 
 * @returns false if userName is  empty else true
 */
function CheckValidUserName(userName){
	if(userName == null || typeof userName != "string"){
		return false;
	}
	if(userName.length == 0){
		return  false;
	}
	return true;
}

/**
 * @returns true if the index is valid to insert at it  else false
 */
function validIndexToInsertAt(){
	let indexToinsertAt = document.getElementById('InsertAtindex').value;

	if(indexToinsertAt == null || isNaN(indexToinsertAt) ){
		return false;
	}
	
	let indexAsNumber = parseInt(indexToinsertAt);
	if(indexAsNumber <=0 || indexAsNumber > usersObject.length){
		return false;
	}

	return true;
}